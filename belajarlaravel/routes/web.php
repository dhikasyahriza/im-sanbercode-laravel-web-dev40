<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'utama']);


Route::get('/register',[AuthController::class, 'register']);

Route::post('/welcome',[AuthController::class, 'welcome']);
Route::get('/data-tables',function(){
    return view ('halaman.data-table');
});
Route::get('/table',function(){
    return view ('halaman.table');
});


//crud 
// create 
// form tambah cast
Route::get('/cast/create',[castController::class, 'create']);
// buat kirim data ke database
Route::post('/cast',[castController::class, 'store']);

//read 
// tampil data 
route::get('/cast',[castController::class,'index']);
//tampil detail 
route::get('/cast/{cast_id}',[castController::class,'show']);

//update
//form update data
route::get('/cast/{cast_id}/edit',[castController::class,'edit']);
//update database terbaru
route::put('/cast/{cast_id}',[castController::class,'update']);

//delete
//delete berdasarkan id
route::delete('/cast/{cast_id}',[castController::class,'destroy']);