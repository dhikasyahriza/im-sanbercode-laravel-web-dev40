@extends('layout.master')
@section('judul')
Buat Account Baru!    
@endsection
@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome"method="post">
    @csrf
<label>First name:</label><br><br>
<input type="text" name = "nama1"><br><br>
<label>Last name:</label><br><br>
<input type="text" name = "nama2"><br><br>
<label>Gender:</label><br><br>
<input type="radio"name="gender">Male <br>
<input type="radio"name="gender">female <br>
<input type="radio"name="gender">Other <br><br>
<label>nationality</label><br>
<select name="nationality"><br>
    <option value="">Indonesian</option>
    <option value="">Singapore</option>
    <option value="">Malaysian</option>
    <option value="">Australian</option>
</select><br><br>
<label>Language Spoken</label><br>
<input type="checkbox" name="language">Bahasa Indonesia <br>
<input type="checkbox" name="language">English <br>
<input type="checkbox" name="language">Other <br><br>
<label>Bio:</label><br><br>
<textarea cols="30" rows="10"></textarea> <br>
<input type="submit"value="Sign Up">

</form>

@endsection
