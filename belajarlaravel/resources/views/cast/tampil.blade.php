@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection
@section('content')
    <a href="/cast/create"class="btn btn-primary btn-sm mb-3">Tambah Cast</a>

    <table class="table table-dark">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nomor</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
    @forelse ($cast as $key => $value)
    <tr>
        <td>{{$key+1}}</td>
        <td>{{$value->nama}}</td>
        <td>
            <form action="/cast/{{$value->id}}"method="POST">
                @csrf
                @method('DELETE')
                <a href="/cast/{{$value->id}}"class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$value->id}}/edit"class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
        </td>
        
    </tr>
    @empty
    <tr>
        <td>
            TIDAK ADA DATA
        </td>
    </tr>
@endforelse
        </tbody>
      </table>
@endsection